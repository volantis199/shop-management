﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;

namespace ShopManagement.E17CN01.Controllers.Data
{
    class DataController
    {
        private static SqlConnection _sqlCon = new SqlConnection();
        
        private static DataController instance;
        public static DataController Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new DataController();
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public DataController() {}

        /// <summary>
        /// Mở kết nối tới database
        /// </summary>
        private void OpenConnection()
        {
            //string conString = @"Data Source=DESKTOP-PQGL9RC\SQLEXPRESS;Initial Catalog=ShopManagement;Integrated Security=True";
            //_sqlCon = new SqlConnection(conString);
            _sqlCon.ConnectionString = ConfigurationManager.ConnectionStrings["ShopManagement"].ConnectionString;
            try
            {
                _sqlCon.Open();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Đóng kết nối database
        /// </summary>
        private void CloseConnection()
        {
            //if the connection is open then close
            //else do nothing
            if (_sqlCon.State == ConnectionState.Open)
                _sqlCon.Close();
        }

        /// <summary>
        /// Hàm xử lý câu query để trả lại dữ liệu lên một bảng
        /// </summary>
        /// <param name="query">Câu truy vấn</param>
        /// <param name="parameters">Tham số truyền vào nếu không có mặc định là null</param>
        /// <returns></returns>
        public DataTable ExecuteQuery(string query, object[] parameters = null)

        {
            DataTable table = new DataTable();
            try
            {
                OpenConnection();
                SqlCommand cmd = new SqlCommand(query, _sqlCon);
                int i = 0;
                if(parameters != null)
                {
                    string[] listParameter = query.Split(' ');
                    foreach (var item in listParameter)
                    {
                        if(item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, parameters[i]);
                            ++i;
                        }
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(table);
                CloseConnection();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return table;
        }

        /// <summary>
        /// Hàm trả về số lượng dòng dữ liệu chịu ảnh hưởng
        /// </summary>
        /// <author>Hoàng Hà</author>
        /// <param name="query">Câu truy vấn</param>
        /// <param name="parameters"> Tham số truyền vào mặc định là null</param>
        /// <returns></returns>
        public int ExecuteNonQuery(string query, object[] parameters = null)
        {
            //number of affected lines
            int line = 0;OpenConnection();
            try
            {
                
                SqlCommand cmd = new SqlCommand(query, _sqlCon);
                int i = 0;
                if(parameters != null)
                {
                    string[] listParameters = query.Split(' ');
                    foreach (var item in listParameters)
                    {
                        if(item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, parameters[i]);
                            ++i;
                        }
                    }
                }

                line = cmd.ExecuteNonQuery();
                CloseConnection();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return line;
        }

        /// <summary>
        /// Hàm xử lý query trả về số dòng kết quả
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parametters"></param>
        /// <returns></returns>
        public object ExecuteScalar(string query, object[] parametters = null)
        {
            object ob = 0;
            try
            {
                OpenConnection();
                SqlCommand cmd = new SqlCommand(query, _sqlCon);
                int i = 0;
                if (parametters != null)
                {
                    string[] listPara = query.Split(' ');
                    foreach (var item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, parametters[i]);
                            ++i;
                        }
                    }
                }
                ob = cmd.ExecuteScalar();
                CloseConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ob;
        }
    }
}
