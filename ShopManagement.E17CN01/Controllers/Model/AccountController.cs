﻿using ShopManagement.E17CN01.Controllers.Data;
using ShopManagement.E17CN01.Models;
using System;
using System.Data;

namespace ShopManagement.E17CN01.Controllers.ModelController
{
    public class AccountController
    {
        public AccountController() { }
        private static AccountController instance;

        public static AccountController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AccountController();

                }
                return instance;
            }

            private set => instance = value;
        }

        public bool LoginSuccessfully(string userName, string password)
        {
            string query = "UserLogin @userName , @password ";
            var result = DataController.Instance.ExecuteQuery(query, new object[] { userName, password});
            return result.Rows.Count > 0;
        }

        public Account GetAccount(string userName, string password)
        {
            string query = "select * from Account where Name = '" + userName + "' and Password = '" + password + "'";
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                return new Account(item);
            }

            return null;
        }

        public void ChangePass(string id, string newPass)
        {
            string query = "EXEC ChangePass @id , @newPass";
            DataController.Instance.ExecuteNonQuery(query, new object[] { id, newPass });
        }
    }
}
