﻿using ShopManagement.E17CN01.Controllers.Data;
using ShopManagement.E17CN01.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class AccountTypeController
    {
        private static AccountTypeController instance;

        public AccountTypeController()
        {

        }

        public static AccountTypeController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AccountTypeController();

                }
                return instance;
            }

            private set => instance = value;
        }

        public AccountType GetAccountType(string id)
        {
            string query = "SELECT * FROM dbo.AccountType WHERE Id = '" + id + "'";
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                return new AccountType(item);
            }

            return null;
        }
    }
}
