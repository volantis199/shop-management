﻿using ShopManagement.E17CN01.Controllers.Data;
using ShopManagement.E17CN01.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class BillController
    {
        private static BillController instance;

        public static BillController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillController();
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public DataTable DisplayBill(string IdBill)
        {
            string query = "EXEC DisplayBill @IdBill";
            return DataController.Instance.ExecuteQuery(query, new object[] { IdBill });
        }

        public List<Bill> LoadBillByDate(DateTime fromValue, DateTime toValue)
        {
            string query = "EXEC DisplayBillByDate @fromValue , @toValue";
            DataTable table =  DataController.Instance.ExecuteQuery(query, new object[] { fromValue, toValue });
            List<Bill> list = new List<Bill>();
            foreach (DataRow item in table.Rows)
            {
                Bill bill = new Bill(item);
                list.Add(bill);
            }
            return list;
        }

        public List<Bill> GetBillListByDateAndPage(DateTime value1, DateTime value2, int pageNum)
        {
            string query = "EXEC GetListBillByDateAndPage @fromDate , @toDate , @page";
            List<Bill> list = new List<Bill>();
            DataTable  table =  DataController.Instance.ExecuteQuery(query, new object[] { value1, value2, pageNum });

            foreach  (DataRow item in table.Rows)
            {
                list.Add(new Bill(item));
            }

            return list;
        }

        public int GetBillPageByDate(DateTime value1, DateTime value2)
        {
            return (int)DataController.Instance.ExecuteScalar("EXEC GetBillPageByDate @fromDate , @toDate", new object[] { value1, value2 });
        }

        public void InsertBill(string idStaff, string idCustomer)
        {
            string query = "EXEC InsertBill @idStaff , @idCustomer";
            DataController.Instance.ExecuteNonQuery(query, new object[] { idStaff, idCustomer});
        }

        public void DeleteBill(int id)
        {
            string query = "DELETE dbo.Bill WHERE Id = '" + id + "'";
            DataController.Instance.ExecuteNonQuery(query);
        }

        public Bill GetNewestBill()
        {
            string query = "SELECT * from dbo.Bill WHERE Id = (SELECT MAX(Id) FROM dbo.Bill)";
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                return new Bill(item);
            }
            return null;
        }

        public void CheckOutBill(double idBill, double total, double discount,  DateTime? checkOutTime)
        {
            string query = "EXEC CheckOutBill @idBill , @total , @discount , @checkOutTime";
            DataController.Instance.ExecuteNonQuery(query, new object[] { idBill, total, discount, checkOutTime });
        }
    }
}
