﻿using ShopManagement.E17CN01.Controllers.Data;
using ShopManagement.E17CN01.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class BillInfoController
    {
        public BillInfoController() { }
        private static BillInfoController instance;
        public static BillInfoController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillInfoController();
                }
                return instance;
            }
            private set => instance = value;
        }

        public void InsertBillInfo(int idBill, string idProduct, int sellPrice, int quantity)
        {
            DataController.Instance.ExecuteQuery("EXEC InsertBillInfo @idBill , @idProduct , @quantity , @sellPrice", new object[] { idBill, idProduct, sellPrice, quantity});
        }

        public List<BillInfo> GetListBillInfoByBillId(int billId)
        {
            List<BillInfo> list = new List<BillInfo>();
            string query = "EXEC GetListBillInfoByBillId @billId";
            DataTable table = DataController.Instance.ExecuteQuery(query,new object[] { billId });

            foreach (DataRow item in table.Rows)
            {
                BillInfo bi = new BillInfo(item);
                list.Add(bi);
            }
            
            return list;
        }

        public void DeleteBillInfoByBillId(int id)
        {
            string query = "DELETE dbo.BillInfo WHERE IdBill = '" + id + "'";
            DataController.Instance.ExecuteNonQuery(query);
        }
    }
}
