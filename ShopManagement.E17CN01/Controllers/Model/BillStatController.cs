﻿using ShopManagement.E17CN01.Controllers.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class BillStatController
    {
        public BillStatController() { }

        private static BillStatController instance;

        public static BillStatController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillStatController();
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public List<BillStat> GetBillStatListByBillId(int idBill)
        {
            List<BillStat> list = new List<BillStat>();
            string query = "EXEC GetBillStatByBillId @idBill";
            DataTable table = DataController.Instance.ExecuteQuery(query, new object[] { idBill });
            foreach (DataRow item in table.Rows)
            {
                BillStat bs = new BillStat(item);
                list.Add(bs);
            }

            return list;
        }
    }
}
