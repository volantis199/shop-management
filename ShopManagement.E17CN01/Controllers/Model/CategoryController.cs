﻿using System.Collections.Generic;
using System.Data;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class CategoryController
    {
        private static CategoryController instance;

        private CategoryController() { }

        public static CategoryController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CategoryController();

                }
                return instance;
            }

            private set => instance = value;
        }
        
        public List<Category> LoadCategory()
        {
            List<Category> list = new List<Category>();
            string query = "select * from dbo.Category";
            DataTable table = Data.DataController.Instance.ExecuteQuery(query);
            if(table != null)
            {
                foreach (DataRow item in table.Rows)
                {
                    Category category = new Category(item);
                    list.Add(category);
                }

                return list;
            }
            else
                return null;
        }
    }
}
