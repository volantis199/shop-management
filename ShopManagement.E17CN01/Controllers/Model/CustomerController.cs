﻿using ShopManagement.E17CN01.Controllers.Data;
using ShopManagement.E17CN01.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class CustomerController
    {
        public CustomerController() { }
        private static CustomerController instance;

        public static CustomerController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CustomerController();
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public List<Customer> LoadCustomer()
        {
            List<Customer> list = new List<Customer>();
            string query = "select * from dbo.Customer";
            DataTable table = Data.DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                Customer customer = new Customer(item);
                list.Add(customer);
            }
            return list;
        }

        public bool InsertCustomer(string id, string name, string idType, string phoneNumb, int saving)
        {
            int res = DataController.Instance.ExecuteNonQuery("EXEC InsertCustomer @id , @name , @idType , @phoneNumb , @saving", new object[] { id, name, idType, phoneNumb, saving });
            return res > 0;
        }

        public void UpdateCustomerById(string id, string name, string phone, int saving, string idType)
        {
            string query = string.Format("UPDATE dbo.Customer SET Name = N'{0}', PhoneNumb = '{1}', Saving = {2}, IdType = '{3}' WHERE Id = '{4}'", name, phone, saving, idType, id);
            DataController.Instance.ExecuteNonQuery(query);
        }

        public void DeleteCustomerById(string id)
        {
            string query = "DELETE FROM dbo.Customer WHERE Id = '" + id + "'";
            DataController.Instance.ExecuteNonQuery(query);
        }

        public int GetTotal()
        {
            string query = "SELECT COUNT(*) FROM dbo.Customer";
            int numb = Convert.ToInt32(DataController.Instance.ExecuteScalar(query));
            return numb;
        }

        public Customer GetCustomerById(string id)
        {
            string query = string.Format("SELECT * FROM dbo.Customer WHERE Id = '{0}'", id);
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                return new Customer(item);
            }

            return null;
        }
    }
}
