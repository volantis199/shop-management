﻿using ShopManagement.E17CN01.Controllers.Data;
using ShopManagement.E17CN01.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class CustomerTypeController
    {
        public CustomerTypeController() { }

        private static CustomerTypeController instance;

        public static CustomerTypeController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CustomerTypeController();
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public List<CustomerType> LoadCustomerType()
        {
            List<CustomerType> list = new List<CustomerType>();

            string query = "SELECT * FROM dbo.CustomerType";
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                CustomerType cus = new CustomerType(item);
                list.Add(cus);
            }

            return list;
        }

        public int GetDiscount(string id)
        {
            string query = "SELECT Discount FROM dbo.CustomerType WHERE Id = '" + id + "'";
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                return Convert.ToInt32(item[0]);
            }
            return 0;
        }

        public CustomerType GetTypeById(string id)
        {
            string query = "SELECT * FROM dbo.CustomerType WHERE Id = '" + id + "'";
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach (DataRow item in table.Rows)
            {
                return new CustomerType(item);
            }

            return null;
        }
    }
}
