﻿using ShopManagement.E17CN01.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class ProductController
    {
        private ProductController() { }
        private static ProductController instance;

        public static ProductController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductController();

                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public List<Product> LoadProductByCategoryId(string id)
        {
            List<Product> list = new List<Product>();
            string query = String.Format("select * from dbo.Product where IdCategory = '{0}'", id);
            DataTable table = Data.DataController.Instance.ExecuteQuery(query);
            if (table != null)
            {
                foreach (DataRow item in table.Rows)
                {
                    Product product = new Product(item);
                    if(product.Status != "Hết hàng")
                        list.Add(product);
                }
                return list;
            }
            else
                return null;
        }
    }
}
