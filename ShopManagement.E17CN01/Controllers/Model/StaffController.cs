﻿using ShopManagement.E17CN01.Controllers.Data;
using ShopManagement.E17CN01.Models;
using System.Data;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class StaffController
    {
        public StaffController() { }

        private static StaffController instance;

        public static StaffController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StaffController();

                }
                return instance;
            }

            private set => instance = value;
        }

        public Staff GetStaffByAccId(string idAcc)
        {
            string query = "select * from Staff where IdAccount = '" + idAcc + "'";
            DataTable table = DataController.Instance.ExecuteQuery(query);
            foreach(DataRow item in table.Rows)
            {
                return new Staff(item);
            }
            return null;
        }

        public void UpdateStaffInfo(string address, string phone, string id)
        {
            string query = "EXEC UpdateStaffInfo @address , @phone , @id";
            DataController.Instance.ExecuteNonQuery(query);
        }
    }
}
