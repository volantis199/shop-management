﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Models
{
    public class Account
    {
        private string id;
        private string userName;
        private string password;
        private string idAccountType;

        public string UserName { get => userName; set => userName = value; }
        public string Password { get => password; set => password = value; }
        public string Id { get => id; set => id = value; }
        public string IdAccountType { get => idAccountType; set => idAccountType = value; }

        public Account() { }

        public Account(DataRow dataRow)
        {
            id = dataRow["Id"].ToString();
            userName = dataRow["Name"].ToString();
            password = dataRow["Password"].ToString();
            idAccountType = dataRow["IdAccountType"].ToString();
        }
    }
}
