﻿using System;
using System.Data;

namespace ShopManagement.E17CN01.Models
{
    class Bill
    {
        private int id;
        private int sum;
        private string idStaff;
        private string idCustomer;
        //private string status;
        private int discount;
        private DateTime? dateCheckOut;

        public Bill() { }

        public Bill(DataRow row)
        {
            id = int.Parse(row["Id"].ToString());
            sum = int.Parse(row["Sum"].ToString());
            idStaff = row["IdStaff"].ToString();
            idCustomer = row["IdCustomer"].ToString();
            //status = row["Status"].ToString();
            discount = int.Parse(row["Discount"].ToString());
            var tmpDate = row["DateCheckOut"];
            if(tmpDate.ToString() != "")
            {
                this.dateCheckOut = (DateTime?)tmpDate;
            }
        }


        public int Id { get => id; set => id = value; }
        public int Sum { get => sum; set => sum = value; }
        public string IdStaff { get => idStaff; set => idStaff = value; }
        public string IdCustomer { get => idCustomer; set => idCustomer = value; }
        //public string Status { get => status; set => status = value; }
        public int Discount { get => discount; set => discount = value; }
        public DateTime? DateCheckOut { get => dateCheckOut; set => dateCheckOut = value; }
    }
}
