﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Models
{
    class BillInfo
    {
        public BillInfo() { }
        public BillInfo(DataRow row)
        {
            id = int.Parse(row["Id"].ToString());
            idBill = int.Parse(row["IdBill"].ToString());
            idProduct = row["IdProduct"].ToString();
            quantity = int.Parse(row["Quantity"].ToString());
        }

        private int id;
        private int idBill;
        private string idProduct;
        private int quantity;

        public int Id { get => id; set => id = value; }
        public int IdBill { get => idBill; set => idBill = value; }
        public string IdProduct { get => idProduct; set => idProduct = value; }
        public int Quantity { get => quantity; set => quantity = value; }
    }
}
