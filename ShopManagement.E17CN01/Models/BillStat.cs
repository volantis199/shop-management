﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class BillStat
    {
        public BillStat() { }

        public BillStat(DataRow row)
        {
            idProduct = row["IdProduct"].ToString();
            name = row["Name"].ToString();
            sellPrice = int.Parse(row["SellPrice"].ToString());
            quantity = int.Parse(row["Quantity"].ToString());
        }

        public BillStat(string idProduct, string name, int sellPrice, int quantity)
        {
            this.idProduct = idProduct;
            this.name = name;
            this.sellPrice = sellPrice;
            this.quantity = quantity;
        }

        private string idProduct;
        private string name;
        private int sellPrice;
        private int quantity;

        public string IdProduct { get => idProduct; set => idProduct = value; }
        public string Name { get => name; set => name = value; }
        public int SellPrice { get => sellPrice; set => sellPrice = value; }
        public int Quantity { get => quantity; set => quantity = value; }
    }
}
