﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class Category
    {
        private string id;
        private string name;

        public Category() { }
        public Category(DataRow row)
        {
            id = row["Id"].ToString();
            name = row["Name"].ToString();
        }

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
    }
}
