﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Controllers.Model
{
    class Customer
    {
        private string id;
        private string name;
        private string idType;
        private string phoneNumb;
        private int saving;

        public Customer() { }
        
        public Customer(DataRow row)
        {
            id = row["Id"].ToString();
            name = row["Name"].ToString();
            idType = row["IdType"].ToString();
            phoneNumb = row["PhoneNumb"].ToString();
            saving = int.Parse(row["Saving"].ToString());
        }


        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string IdType { get => idType; set => idType = value; }
        public string PhoneNumb { get => phoneNumb; set => phoneNumb = value; }
        public int Saving { get => saving; set => saving = value; }
    }
}
