﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Models
{
    class CustomerType
    {
        private string id;
        private string name;
        private int discount;
        public CustomerType() { }

        public CustomerType(DataRow row)
        {
            id = row["Id"].ToString();
            name = row["Name"].ToString();
            discount = int.Parse(row["Discount"].ToString());
        }

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Discount { get => discount; set => discount = value; }
    }
}
