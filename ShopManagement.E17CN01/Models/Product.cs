﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Models
{
    class Product
    {
        private string id;
        private string idCategory;
        private string name;
        private string status;
        private string manufacturer;
        private int importPrice;
        private int sellPrice;

        public Product() { }
        
        public Product(DataRow row)
        {
            id = row["Id"].ToString();
            idCategory = row["IdCategory"].ToString();
            name = row["Name"].ToString();
            status = row["Status"].ToString();
            manufacturer = row["Manufacturer"].ToString();
            importPrice = int.Parse(row["ImportPrice"].ToString());
            sellPrice = int.Parse(row["SellPrice"].ToString());
        }

        public string Id { get => id; set => id = value; }
        public string IdCategory { get => idCategory; set => idCategory = value; }
        public string Name { get => name; set => name = value; }
        public string Status { get => status; set => status = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public int ImportPrice { get => importPrice; set => importPrice = value; }
        public int SellPrice { get => sellPrice; set => sellPrice = value; }
    }
}
