﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManagement.E17CN01.Models
{
    class Staff
    {
        public Staff() { }

        public Staff(DataRow row)
        {
            id = row["Id"].ToString();
            name = row["Name"].ToString();
            idPos = row["IdPos"].ToString();
            phoneNumb = row["PhoneNumb"].ToString();
            address = row["Address"].ToString();
            idAccount = row["IdAccount"].ToString();
        }

        private string id;
        private string name;
        private string idPos;
        private string phoneNumb;
        private string address;
        private string idAccount;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string IdPos { get => idPos; set => idPos = value; }
        public string PhoneNumb { get => phoneNumb; set => phoneNumb = value; }
        public string Address { get => address; set => address = value; }
        public string IdAccount { get => idAccount; set => idAccount = value; }
    }
}
