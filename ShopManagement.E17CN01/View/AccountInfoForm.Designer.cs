﻿namespace ShopManagement.E17CN01.View
{
    partial class AccountInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountInfoForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txbName = new System.Windows.Forms.TextBox();
            this.txbPhoneNumb = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.txbAddress = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txbId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txbUser = new System.Windows.Forms.TextBox();
            this.txbOldPass = new System.Windows.Forms.TextBox();
            this.txbNewPass = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txbReenter = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ShopManagement.E17CN01.Properties.Resources.Hacker_icon;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(284, 280);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số điện thoại:";
            // 
            // txbName
            // 
            this.txbName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbName.Enabled = false;
            this.txbName.Location = new System.Drawing.Point(190, 17);
            this.txbName.Name = "txbName";
            this.txbName.Size = new System.Drawing.Size(256, 24);
            this.txbName.TabIndex = 2;
            this.txbName.TabStop = false;
            // 
            // txbPhoneNumb
            // 
            this.txbPhoneNumb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbPhoneNumb.Enabled = false;
            this.txbPhoneNumb.Location = new System.Drawing.Point(190, 66);
            this.txbPhoneNumb.Name = "txbPhoneNumb";
            this.txbPhoneNumb.Size = new System.Drawing.Size(256, 24);
            this.txbPhoneNumb.TabIndex = 3;
            this.txbPhoneNumb.TabStop = false;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(16, 95);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(74, 23);
            this.label.TabIndex = 1;
            this.label.Text = "Địa chỉ:";
            // 
            // txbAddress
            // 
            this.txbAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbAddress.Enabled = false;
            this.txbAddress.Location = new System.Drawing.Point(190, 105);
            this.txbAddress.Name = "txbAddress";
            this.txbAddress.Size = new System.Drawing.Size(256, 24);
            this.txbAddress.TabIndex = 3;
            this.txbAddress.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(589, 364);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(172, 34);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Huỷ";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(335, 365);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(172, 34);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Đổi mật khẩu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên:";
            // 
            // txbId
            // 
            this.txbId.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txbId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbId.Enabled = false;
            this.txbId.Location = new System.Drawing.Point(11, 300);
            this.txbId.Name = "txbId";
            this.txbId.Size = new System.Drawing.Size(284, 24);
            this.txbId.TabIndex = 3;
            this.txbId.TabStop = false;
            this.txbId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(331, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 23);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mật khẩu cũ:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(331, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 23);
            this.label4.TabIndex = 1;
            this.label4.Text = "Tên đăng nhập:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(331, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 23);
            this.label5.TabIndex = 1;
            this.label5.Text = "Mật khẩu mới:";
            // 
            // txbUser
            // 
            this.txbUser.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txbUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbUser.Enabled = false;
            this.txbUser.Location = new System.Drawing.Point(505, 194);
            this.txbUser.Name = "txbUser";
            this.txbUser.Size = new System.Drawing.Size(256, 24);
            this.txbUser.TabIndex = 2;
            this.txbUser.TabStop = false;
            // 
            // txbOldPass
            // 
            this.txbOldPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbOldPass.Location = new System.Drawing.Point(505, 237);
            this.txbOldPass.Name = "txbOldPass";
            this.txbOldPass.Size = new System.Drawing.Size(256, 24);
            this.txbOldPass.TabIndex = 3;
            this.txbOldPass.TabStop = false;
            this.txbOldPass.UseSystemPasswordChar = true;
            // 
            // txbNewPass
            // 
            this.txbNewPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbNewPass.Location = new System.Drawing.Point(505, 274);
            this.txbNewPass.Name = "txbNewPass";
            this.txbNewPass.Size = new System.Drawing.Size(256, 24);
            this.txbNewPass.TabIndex = 3;
            this.txbNewPass.TabStop = false;
            this.txbNewPass.UseSystemPasswordChar = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(331, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(170, 23);
            this.label6.TabIndex = 1;
            this.label6.Text = "Nhập lại mật khẩu:";
            // 
            // txbReenter
            // 
            this.txbReenter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbReenter.Location = new System.Drawing.Point(505, 321);
            this.txbReenter.Name = "txbReenter";
            this.txbReenter.Size = new System.Drawing.Size(256, 24);
            this.txbReenter.TabIndex = 3;
            this.txbReenter.TabStop = false;
            this.txbReenter.UseSystemPasswordChar = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txbAddress);
            this.panel1.Controls.Add(this.txbPhoneNumb);
            this.panel1.Controls.Add(this.txbName);
            this.panel1.Controls.Add(this.label);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(315, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(462, 152);
            this.panel1.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(501, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(195, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "Thông tin người dùng";
            // 
            // AccountInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 410);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txbId);
            this.Controls.Add(this.txbReenter);
            this.Controls.Add(this.txbNewPass);
            this.Controls.Add(this.txbOldPass);
            this.Controls.Add(this.txbUser);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "AccountInfoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbName;
        private System.Windows.Forms.TextBox txbPhoneNumb;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox txbAddress;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbUser;
        private System.Windows.Forms.TextBox txbOldPass;
        private System.Windows.Forms.TextBox txbNewPass;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbReenter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
    }
}