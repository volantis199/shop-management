﻿using ShopManagement.E17CN01.Controllers.Model;
using ShopManagement.E17CN01.Controllers.ModelController;
using ShopManagement.E17CN01.Models;
using System.Windows.Forms;

namespace ShopManagement.E17CN01.View
{
    public partial class AccountInfoForm : Form
    {
        private Account localAccount;
        public Account LocalAccount { get => localAccount; set => localAccount = value; }
        private Staff staff;

        public AccountInfoForm(Account acc)
        {
            localAccount = acc;
            InitializeComponent();
            LoadFormComponent();
        }

        private Staff LoadStaff(string idAcc)
        {
            return StaffController.Instance.GetStaffByAccId(idAcc);
        }

        //private void LoadByAccountType()
        //{
        //    accType = AccountTypeController.Instance.GetAccountType(localAccount.IdAccountType).Name;
        //    if (accType == "admin")
        //    {
        //        txbOldPass.Enabled = txbNewPass.Enabled = txbReenter.Enabled = false;
        //        txbName.Enabled = txbAddress.Enabled = txbPhoneNumb.Enabled = true;
        //    }
        //    else
        //    {
        //        txbOldPass.Enabled = txbNewPass.Enabled = txbReenter.Enabled = true;
        //        txbAddress.Enabled = txbPhoneNumb.Enabled = false;
        //    }
        //}
        private void LoadFormComponent()
        {
            staff = LoadStaff(localAccount.Id);
            txbId.Text = staff.Id;
            txbName.Text = staff.Name;
            txbPhoneNumb.Text = staff.PhoneNumb;
            txbAddress.Text = staff.Address;

            txbUser.Text = localAccount.UserName;
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            string pass = txbOldPass.Text.Trim();
            string newPass = txbNewPass.Text.Trim();
            string reenter = txbReenter.Text.Trim();
            if (newPass != reenter)
            {
                MessageBox.Show("Vui lòng kiểm tra lại mật khẩu mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txbOldPass.Text = txbReenter.Text = txbNewPass.Text = string.Empty;
                return;
            }
            AccountController.Instance.ChangePass(localAccount.Id, newPass);
            MessageBox.Show("Cập nhật mật khẩu thành công", "Thông báo");
            txbOldPass.Text = txbReenter.Text = txbNewPass.Text = string.Empty;
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
