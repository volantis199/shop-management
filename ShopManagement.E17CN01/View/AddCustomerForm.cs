﻿using ShopManagement.E17CN01.Controllers.Model;
using ShopManagement.E17CN01.Models;
using System;
using System.Windows.Forms;

namespace ShopManagement.E17CN01.View
{
    partial class AddCustomerForm : Form
    {
        #region Settings
        #endregion

        public AddCustomerForm()
        {
            InitializeComponent();
            LoadCustomerType();
        }

        private string GenerateId()
        {
            int no = CustomerController.Instance.GetTotal() + 1;
            int[] id = new int[8];
            for(int i = 7; i >= 0; ++i)
            {
                if (no > 0)
                {
                    int tmp = no % 10;
                    id[i] = tmp;
                    no /= 10;
                }
                else
                    break;
            }
            string res = "CU";
            for(int i = 0; i < 8; ++i)
            {
                res += id[i].ToString();
            }
            return res;
        }

        private void LoadCustomerType()
        {
            cbCustomerType.DataSource = CustomerTypeController.Instance.LoadCustomerType();
            cbCustomerType.DisplayMember = "Name";
        }

        private void BtnUpload_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Có lỗi, vui lòng thử lại sau!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if(txbName.Text.ToString() == " " || txbPhoneNumb.Text.ToString() == " ")
            {
                MessageBox.Show("Vui lòng kiểm tra lại đầy đủ thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string Id = GenerateId();
                string Name = txbName.Text.ToString();
                string IdType = (cbCustomerType.SelectedItem as CustomerType).Id;
                string PhoneNumb = txbPhoneNumb.Text.ToString();
                if(CustomerController.Instance.InsertCustomer(Id, Name, IdType, PhoneNumb, 0))
                {
                    MessageBox.Show("Thêm mới thành công!", "Thông báo");
                    txbName.Text = txbPhoneNumb.Text = string.Empty;
                }
            }

        }
    }
}
