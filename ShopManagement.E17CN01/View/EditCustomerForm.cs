﻿using ShopManagement.E17CN01.Controllers.Model;
using ShopManagement.E17CN01.Models;
using System;
using System.Windows.Forms;

namespace ShopManagement.E17CN01.View
{
    public partial class EditCustomer : Form
    {
        BindingSource customerBinding = new BindingSource();

        public EditCustomer()
        {
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            dtgvCustomer.DataSource = customerBinding;
            LoadCustomerList();
            LoadCustomerType(cbType);
            AddCustomerBinding();
        }

        private void LoadCustomerType(ComboBox cb)
        {
            cb.DataSource = CustomerTypeController.Instance.LoadCustomerType();
            cb.DisplayMember = "Name";
        }

        private void LoadCustomerList()
        {
            customerBinding.DataSource = CustomerController.Instance.LoadCustomer();
        }

        private void AddCustomerBinding()
        {
            txbId.DataBindings.Add(new Binding("Text", dtgvCustomer.DataSource, "Id"));
            txbName.DataBindings.Add(new Binding("Text", dtgvCustomer.DataSource, "Name"));
            txbPhoneNumb.DataBindings.Add(new Binding("Text", dtgvCustomer.DataSource, "PhoneNumb"));
            txbSaving.DataBindings.Add(new Binding("Text", dtgvCustomer.DataSource, "Saving"));
        }

        private void TxbId_TextChanged(object sender, EventArgs e)
        {
            //lấy ra giá trị của idType
            string idType = dtgvCustomer.SelectedCells[0].OwningRow.Cells["IdType"].Value.ToString();
            CustomerType type = CustomerTypeController.Instance.GetTypeById(idType);

            int index = -1;
            int i = 0;
            foreach (CustomerType item in cbType.Items)
            {
                if (item.Id == type.Id)
                {
                    index = i;
                    break;
                }
                i++;
            }

            cbType.SelectedIndex = index;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string id = txbId.Text.Trim();
            string name = txbName.Text.Trim();
            string phone = txbPhoneNumb.Text.Trim();
            int saving = Convert.ToInt32(txbSaving.Text.Trim());
            string idType = (cbType.SelectedItem as CustomerType).Id;

            CustomerController.Instance.UpdateCustomerById(id, name, phone, saving, idType);
            MessageBox.Show("Cập nhật thông tin thành công!", "Thông báo");

            //if(updateInfo != null)
            //{
            //    updateInfo(this, new EventArgs());
            //}
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bạn không được phép!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //string id = txbId.Text.Trim();

            //CustomerController.Instance.DeleteCustomerById(id);
        }

        private void pBInfo_MouseHover(object sender, EventArgs e)
        {
            MessageBox.Show("Broze: thành viên đồng - discount: 0% \n " +
                "Silver: thành viên bạc - discount: 5% \n" +
                "Gold: thành viên vàng - discount 10% \n", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //private event EventHandler updateInfo;
        //public event EventHandler UpdateInfo
        //{
        //    add
        //    {
        //        updateInfo += value;
        //    }
        //    remove
        //    {
        //        updateInfo -= value;
        //    }
        //}

    }
}
