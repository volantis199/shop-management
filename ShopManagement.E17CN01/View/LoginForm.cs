﻿using ShopManagement.E17CN01.Controllers.ModelController;
using ShopManagement.E17CN01.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopManagement.E17CN01.View
{
    public partial class loginForm : Form
    {
        public loginForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sự kiện xảy ra khi ấn nút Login => Load form quản lý
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text.Trim();
            string password = txbPassword.Text.Trim();
            if(Login(userName, password))
            {
                Account loginAccount = AccountController.Instance.GetAccount(userName, password);
                mainForm f = new mainForm(loginAccount);
                this.Hide();
                f.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("Đăng nhập không thành công, kiểm tra lại!");
            }
        }

        /// <summary>
        /// Hàm kiểm tra đăng nhập thành công hay không
        /// </summary>
        /// <param name="userName">Tên đăng nhập</param>
        /// <param name="password">Mật khẩu</param>
        /// <returns></returns>
        private bool Login(string userName, string password)
        {
            return AccountController.Instance.LoginSuccessfully(userName, password);
        }

        /// <summary>
        /// Sự kiện xảy ra khi ấn nút Exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hàm xử lý khi ấn nút "x" tắt form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Bạn có thực sự muốn thoát?", "Thông báo", MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                e.Cancel = true;
            }
        }
    }
}
