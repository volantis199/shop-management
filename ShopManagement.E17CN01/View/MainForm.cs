﻿using System;
using ShopManagement.E17CN01.Models;
using System.Windows.Forms;
using ShopManagement.E17CN01.Controllers.Model;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using ShopManagement.E17CN01.View;

namespace ShopManagement.E17CN01
{
    public partial class mainForm : Form
    {
        #region Initialize
        private Account userAccount;
        public Account UserAccount { get => userAccount; set => userAccount = value; }

        private Bill currentBill;
        private Staff staff;

        public mainForm(Account loginAccount)
        {
            
            InitializeComponent();
            LoadBasicInfo(loginAccount);
        }

        #region Load Data
        private void LoadAccount(Account acc)
        {
            this.userAccount = acc;
            txbStaffName.Text = StaffController.Instance.GetStaffByAccId(acc.Id).Name;
        }

        private void LoadStaff()
        {
            staff = StaffController.Instance.GetStaffByAccId(userAccount.Id);
        }

        private void LoadCustomer(ComboBox cb)
        {
            cb.DataSource = CustomerController.Instance.LoadCustomer();
            cb.DisplayMember = "Name";
        }

        private void LoadCategory(ComboBox cb)
        {
            cb.DataSource = CategoryController.Instance.LoadCategory();
            cb.DisplayMember = "Name";
        }

        private void LoadProductByCategoryId(string Id)
        {
            cbProduct.DataSource = ProductController.Instance.LoadProductByCategoryId(Id);
            cbProduct.DisplayMember = "Name";
        }

        private void LoadNewBill()
        {
            string staffId = staff.Id;
            BillController.Instance.InsertBill(staffId, (cbCustomer.SelectedItem as Customer).Id);
            currentBill = BillController.Instance.GetNewestBill();
        }

        private void ResetBill()
        {
            LoadNewBill();
            LoadBillToTable();
        }

        private void ResetForm()
        {
            nmQuantity.Value = 1;
            txbDiscount.Enabled = false;
            txbDiscount.Text = "0";
        }

        private void LoadBasicInfo(Account acc)
        {
            LoadAccount(acc);
            LoadStaff();
            LoadCategory(cbCategory);
            LoadCustomer(cbCustomer);
            LogOutToolStripMenuItem.Text += " (" + userAccount.UserName + ")";
        }

        private void LoadBillToTable()
        {
            //bug return null
            List<BillStat> list = BillStatController.Instance.GetBillStatListByBillId(currentBill.Id);

            dtgvBill.DataSource = list;
            float totalPrice = 0;

            foreach (BillStat item in list)
            {
                totalPrice += item.Quantity * item.SellPrice;
            }

            CultureInfo culture = new CultureInfo("vi-VN");
            //set thread hiện tại về culture
            Thread.CurrentThread.CurrentCulture = culture;
            txbTotalPrice.Text = totalPrice.ToString("c", culture); //c: currency
        }

        #endregion

        #endregion


        #region Events

        private void BtnDiscount_Click(object sender, EventArgs e)
        {
            if(txbDiscount.Enabled == false)
            {
                txbDiscount.Enabled = true;
            }
            else
            {
                txbDiscount.Text = " ";
                txbDiscount.Enabled = false;
            }
        }

        private void BtnAddProduct_Click(object sender, EventArgs e)
        {
            if (currentBill == null)
            {
                LoadNewBill();
            }
            int idBill = currentBill.Id;
            string idProduct = (cbProduct.SelectedItem as Product).Id;
            int sellPrice = (cbProduct.SelectedItem as Product).SellPrice;
            int quantity = (int)nmQuantity.Value;
            BillInfoController.Instance.InsertBillInfo(idBill, idProduct, quantity, sellPrice);
            LoadBillToTable();
            nmQuantity.Value = 1;
        }

        private void CbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Id = "";

            ComboBox cb = sender as ComboBox;
            if (cb.SelectedItem == null)
                return;
            Category selected = cb.SelectedItem as Category;
            Id = selected.Id;

            LoadProductByCategoryId(Id);
        }

        private void LogOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            //int total = int.Parse(txbTotalPrice.ToString());
            //đoạn code lỗi do là txbTotalPrice đang có định dạng kiểu tiền
            if(dtgvBill.DataSource == null)
            {
                MessageBox.Show("Hoá đơn đang trống!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            double idBill = currentBill.Id;
            //string note = txbNote.ToString();
            DateTime? checkOutTime = dtpkBill.Value;

            double total = Convert.ToDouble(txbTotalPrice.Text.Split(',')[0]);
            double discount = double.Parse(txbDiscount.Text.ToString());

            string staffName = txbStaffName.Text.ToString();

            double finalPrice = total * (1 - discount/100);

            string paycheck = String.Format("Bạn có thực sự muốn thanh toán " +
                "cho hoá đơn số {0} vào lúc {1}\n Tổng tiền: {2}, giảm giá: {3}%\n Còn lại: {4} " +
                "\n Nhân viên thanh toán: {5}", idBill, checkOutTime, total, discount, finalPrice, staffName);

            if (MessageBox.Show(paycheck,"Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                
                BillController.Instance.CheckOutBill(currentBill.Id, total, discount, checkOutTime);
                ResetForm();
                ResetBill();
            }
            
        }

        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            AddCustomerForm f = new AddCustomerForm();
            f.ShowDialog();
            //Add customer event
        }

        private void CbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            txbPhoneNumb.Text = (cbCustomer.SelectedItem as Customer).PhoneNumb;
            txbDiscount.Text = (CustomerTypeController.Instance.GetDiscount((cbCustomer.SelectedItem as Customer).IdType)).ToString();
        }

        #region Form Events



        #endregion

        #endregion

        private void BtnEditCustomer_Click(object sender, EventArgs e)
        {
            EditCustomer f = new EditCustomer();
            f.ShowDialog();
            //f.UpdateInfo += F_UpdateInfo;
        }

        //private void F_UpdateInfo(object sender, EventArgs e)
        //{
            
        //}

        private void AccountInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountInfoForm f = new AccountInfoForm(userAccount);
            f.ShowDialog();
        }

        private void StatisticToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(userAccount.IdAccountType == "AT01")
            {
                StatisticsForm f = new StatisticsForm(userAccount);
                f.ShowDialog();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Bạn không có quyền truy cập!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void pbReload_Click(object sender, EventArgs e)
        {
            LoadCustomer(cbCustomer);
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (currentBill == null)
                return;
            List<BillInfo> list = BillInfoController.Instance.GetListBillInfoByBillId(currentBill.Id);
            if (list.Count == 0)
            {
                BillController.Instance.DeleteBill(currentBill.Id);
            }
            else
            {
                if((MessageBox.Show("Hiện tại vẫn còn hoá đơn. Bạn có muốn thoát?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK))
                {
                    e.Cancel = true;
                }
                else
                {
                    BillInfoController.Instance.DeleteBillInfoByBillId(currentBill.Id);
                }
            }
        }
    }
}
