﻿using ShopManagement.E17CN01.Controllers.Model;
using ShopManagement.E17CN01.Models;
using System;
using System.Windows.Forms;

namespace ShopManagement.E17CN01.View
{
    public partial class StatisticsForm : Form
    {
        private static int rowPerPage = 10;
        private Account localAccount;
        public Account LocalAccount { get => localAccount; set => localAccount = value; }

        public StatisticsForm(Account acc)
        {
            localAccount = acc;
            InitializeComponent();
            LoadBillStatistic();
        }

        private void LoadBillStatistic()
        {
            dtgvBill.DataSource = BillController.Instance.LoadBillByDate(dtpkFromDate.Value, dtpkToDate.Value);
        }

        private void dtpkFromDate_ValueChanged(object sender, EventArgs e)
        {
            LoadBillStatistic();
        }

        private void btnFirstPage_Click(object sender, EventArgs e)
        {
            txbCurrentPage.Text = "1";
        }

        private void txbCurrentPage_TextChanged(object sender, EventArgs e)
        {
            dtgvBill.DataSource = BillController.Instance.GetBillListByDateAndPage(dtpkFromDate.Value, dtpkToDate.Value, Convert.ToInt32(txbCurrentPage.Text));
        }

        private void dtpkToDate_ValueChanged(object sender, EventArgs e)
        {
            dtgvBill.DataSource = BillController.Instance.GetBillListByDateAndPage(dtpkFromDate.Value, dtpkToDate.Value, Convert.ToInt32(txbCurrentPage.Text));
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            int currentPage = Convert.ToInt32(txbCurrentPage.Text);
            if(currentPage > 1)
            {
                txbCurrentPage.Text = (currentPage - 1).ToString();
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            int currentPage = Convert.ToInt32(txbCurrentPage.Text);
            int totalRow = BillController.Instance.GetBillPageByDate(dtpkFromDate.Value, dtpkToDate.Value);
            int totalPage = 0;
            if (totalRow % rowPerPage != 0)
            {
                totalPage = totalRow/rowPerPage +1;

            }
            else
            {
                totalPage = totalRow / rowPerPage;

            }
            if (currentPage < totalPage)
            {
                currentPage++;
                string tmp = currentPage.ToString();
                txbCurrentPage.Text = tmp;
            }
        }

        private void btnLastPage_Click(object sender, EventArgs e)
        {
            int totalPage = BillController.Instance.GetBillPageByDate(dtpkFromDate.Value, dtpkToDate.Value);
            int billPerPage = 10;
            int lastPage = totalPage / billPerPage;
            if(totalPage % 10 != 0)
            {
                lastPage++;
            }

            txbCurrentPage.Text = lastPage.ToString();
        }
    }
}
