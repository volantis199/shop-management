USE [master]
GO
/****** Object:  Database [ShopManagement]    Script Date: 11/3/2019 10:37:02 AM ******/
CREATE DATABASE [ShopManagement]
GO
USE [ShopManagement]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 11/3/2019 10:37:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [varchar](10) NOT NULL,
	[Name] [varchar](10) NOT NULL,
	[Password] [varchar](max) NOT NULL,
	[IdAccountType] [varchar](10) NOT NULL,
 CONSTRAINT [PK__Account__3214EC076EB2C640] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 11/3/2019 10:37:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[Id] [varchar](10) NOT NULL,
	[Name] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 11/3/2019 10:37:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Sum] [int] NULL,
	[IdStaff] [varchar](10) NOT NULL,
	[IdCustomer] [varchar](10) NOT NULL,
	[Discount] [int] NULL,
	[DateCheckOut] [smalldatetime] NULL,
 CONSTRAINT [PK__Bill__3214EC07697594E2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillInfo]    Script Date: 11/3/2019 10:37:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdBill] [int] NULL,
	[IdProduct] [varchar](10) NOT NULL,
	[Quantity] [int] NULL,
	[SellPrice] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 11/3/2019 10:37:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [varchar](10) NOT NULL,
	[Name] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 11/3/2019 10:37:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [varchar](10) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[IdType] [varchar](10) NOT NULL,
	[PhoneNumb] [varchar](11) NOT NULL,
	[Saving] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerType]    Script Date: 11/3/2019 10:37:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerType](
	[Id] [varchar](10) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Discount] [int] NULL,
 CONSTRAINT [PK__Customer__3214EC07663D9568] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Position]    Script Date: 11/3/2019 10:37:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[Id] [varchar](10) NOT NULL,
	[Name] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 11/3/2019 10:37:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [varchar](10) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[IdCategory] [varchar](10) NOT NULL,
	[Status] [nvarchar](20) NULL,
	[ImportPrice] [int] NULL,
	[SellPrice] [int] NULL,
	[Manufacturer] [nvarchar](20) NULL,
 CONSTRAINT [PK__Product__3214EC07AAEFC234] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salary]    Script Date: 11/3/2019 10:37:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [varchar](10) NULL,
	[IdPos] [varchar](10) NOT NULL,
	[Pay] [int] NOT NULL,
	[Bonus] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 11/3/2019 10:37:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [varchar](10) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[IdPos] [varchar](10) NOT NULL,
	[PhoneNumb] [nvarchar](11) NULL,
	[Address] [nvarchar](20) NULL,
	[IdAccount] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([Id], [Name], [Password], [IdAccountType]) VALUES (N'AC00000002', N'staff', N'1', N'AT02')
INSERT [dbo].[Account] ([Id], [Name], [Password], [IdAccountType]) VALUES (N'AC00000003', N'staff1', N'1', N'AT02')
INSERT [dbo].[Account] ([Id], [Name], [Password], [IdAccountType]) VALUES (N'AC00000004', N'staff2', N'1', N'AT02')
INSERT [dbo].[Account] ([Id], [Name], [Password], [IdAccountType]) VALUES (N'AC00000005', N'staff3', N'1', N'AT02')
INSERT [dbo].[Account] ([Id], [Name], [Password], [IdAccountType]) VALUES (N'AC00000006', N'staff4', N'1', N'AT02')
INSERT [dbo].[Account] ([Id], [Name], [Password], [IdAccountType]) VALUES (N'AC00000007', N'other', N'1', N'AT03')
INSERT [dbo].[Account] ([Id], [Name], [Password], [IdAccountType]) VALUES (N'AC01', N'admin', N'1', N'AT01')
INSERT [dbo].[AccountType] ([Id], [Name]) VALUES (N'AT01', N'admin')
INSERT [dbo].[AccountType] ([Id], [Name]) VALUES (N'AT02', N'staff')
INSERT [dbo].[AccountType] ([Id], [Name]) VALUES (N'AT03', N'others')
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (12, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-26T13:36:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (13, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-23T10:00:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (17, 0, N'ST01', N'CU01', 0, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (18, 0, N'ST01', N'CU01', 0, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (19, 0, N'ST01', N'CU01', 0, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (20, 0, N'ST01', N'CU01', 0, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (21, 0, N'ST01', N'CU01', 0, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (22, 0, N'ST01', N'CU01', 0, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (23, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-24T09:23:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (24, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-24T09:23:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (25, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-24T09:23:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (26, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-24T09:23:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (27, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-24T09:24:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (28, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-24T09:24:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (29, 0, N'ST01', N'CU01', 0, CAST(N'2019-10-24T09:25:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (98, 400000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T16:57:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (99, 400000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T16:57:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (100, 400000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T16:58:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (101, 400000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T17:01:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (102, 400000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T17:03:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (104, 200000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T17:04:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (106, 400000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T17:25:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (109, 2600000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T17:41:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (110, 200000, N'ST01', N'CU01', 0, CAST(N'2019-10-28T17:41:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (112, 600000, N'ST01', N'CU01', 0, CAST(N'2019-10-29T12:24:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (114, 200000, N'ST01', N'CU01', 0, CAST(N'2019-10-29T12:48:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (116, 4400000, N'ST01', N'CU02', 0, CAST(N'2019-10-29T12:50:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (118, 4000000, N'ST01', N'CU02', 0, CAST(N'2019-10-29T12:52:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (121, 13200000, N'ST01', N'CU02', 0, CAST(N'2019-10-29T12:54:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (137, 1120000, N'ST01', N'CU00000003', 0, CAST(N'2019-11-01T22:26:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (139, 680000, N'ST01', N'CU00000003', 0, CAST(N'2019-11-02T23:20:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (140, 0, N'ST01', N'CU00000003', 0, NULL)
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (141, 4000000, N'ST01', N'CU00000003', 0, CAST(N'2019-11-02T23:22:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (142, 0, N'ST01', N'CU00000003', 0, NULL)
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (143, 8600000, N'ST01', N'CU00000003', 0, CAST(N'2019-11-02T23:23:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (144, 0, N'ST01', N'CU00000003', 0, NULL)
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (145, 1200000, N'ST01', N'CU00000003', 0, CAST(N'2019-11-03T10:24:00' AS SmallDateTime))
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (146, 0, N'ST01', N'CU00000004', 0, NULL)
INSERT [dbo].[Bill] ([Id], [Sum], [IdStaff], [IdCustomer], [Discount], [DateCheckOut]) VALUES (147, 1600000, N'ST01', N'CU00000003', 0, CAST(N'2019-11-03T10:27:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Bill] OFF
SET IDENTITY_INSERT [dbo].[BillInfo] ON 

INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (76, 137, N'P01', 1, 200000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (77, 137, N'P02', 4, 230000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (78, 139, N'P02', 2, 230000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (79, 139, N'P03', 1, 220000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (80, 140, N'P03', 20, 220000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (81, 141, N'P01', 20, 200000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (82, 142, N'P02', 20, 230000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (84, 143, N'P02', 20, 230000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (85, 145, N'P000000012', 1, 800000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (86, 145, N'P000000008', 2, 200000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (87, 147, N'P000000012', 2, 800000)
INSERT [dbo].[BillInfo] ([Id], [IdBill], [IdProduct], [Quantity], [SellPrice]) VALUES (83, 143, N'P01', 20, 200000)
SET IDENTITY_INSERT [dbo].[BillInfo] OFF
INSERT [dbo].[Category] ([Id], [Name]) VALUES (N'CA00000003', N'Giày')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (N'CA01', N'Quần')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (N'CA02', N'Áo')
INSERT [dbo].[Customer] ([Id], [Name], [IdType], [PhoneNumb], [Saving]) VALUES (N'CU00000003', N'Hoàng Giang', N'CT03', N'0326251382', 609680000)
INSERT [dbo].[Customer] ([Id], [Name], [IdType], [PhoneNumb], [Saving]) VALUES (N'CU00000004', N'Zed', N'CT01', N'0987678662', 0)
INSERT [dbo].[Customer] ([Id], [Name], [IdType], [PhoneNumb], [Saving]) VALUES (N'CU00000005', N'Lương Đức', N'CT01', N'0375633099', 0)
INSERT [dbo].[Customer] ([Id], [Name], [IdType], [PhoneNumb], [Saving]) VALUES (N'CU01', N'Lại Thanh', N'CT02', N'0382886866', 6240000)
INSERT [dbo].[Customer] ([Id], [Name], [IdType], [PhoneNumb], [Saving]) VALUES (N'CU02', N'Hoàng Hà', N'CT02', N'0987890685', 1740000)
INSERT [dbo].[CustomerType] ([Id], [Name], [Discount]) VALUES (N'CT01', N'Bronze', 0)
INSERT [dbo].[CustomerType] ([Id], [Name], [Discount]) VALUES (N'CT02', N'Silver', 5)
INSERT [dbo].[CustomerType] ([Id], [Name], [Discount]) VALUES (N'CT03', N'Gold', 10)
INSERT [dbo].[Position] ([Id], [Name]) VALUES (N'PO01', N'Nhân viên')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (N'PO02', N'Bảo vệ')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (N'PO03', N'Quản lý')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (N'PO04', N'Cộng tác viên')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000004', N'Quần khaki', N'CA01', N'Còn hàng', 200000, 250000, N'Hàn Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000005', N'Quần bò mài xanh nhạt', N'CA01', N'Còn hàng', 180000, 240000, N'Trung Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000006', N'Quần bò mài đen', N'CA01', N'Hết hàng', 180000, 240000, N'Hàn Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000007', N'Quần bò mài xanh đậm', N'CA01', N'Hết hàng', 180000, 240000, N'Trung Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000008', N'Quần short bò ', N'CA01', N'Còn hàng', 150000, 200000, N'Trung Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000009', N'Áo phông không cổ', N'CA02', N'Còn hàng', 180000, 240000, N'Hàn Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000010', N'Áo sơ mi xanh nhạt', N'CA02', N'Còn hàng', 200000, 260000, N'Trung Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000011', N'Áo phông có cổ', N'CA02', N'Hết hàng', 170000, 230000, N'Trung Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000012', N'Giày Nike  SF', N'CA00000003', N'Còn hàng', 500000, 800000, N'Trung Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P000000013', N'Giày Addidas Sf', N'CA00000003', N'Hết hàng', 600000, 100000, N'Việt Nam')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P01', N'Quần âu', N'CA01', N'Còn hàng', 150000, 200000, N'Trung Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P02', N'Quần bò', N'CA01', N'Còn hàng', 170000, 230000, N'Hàn Quốc')
INSERT [dbo].[Product] ([Id], [Name], [IdCategory], [Status], [ImportPrice], [SellPrice], [Manufacturer]) VALUES (N'P03', N'Áo sơ mi', N'CA02', N'Còn hàng', 160000, 220000, N'Trung Quốc')
INSERT [dbo].[Salary] ([Id], [IdPos], [Pay], [Bonus]) VALUES (N'SL01', N'PO01', 4000000, 10)
INSERT [dbo].[Salary] ([Id], [IdPos], [Pay], [Bonus]) VALUES (N'SL02', N'PO02', 3500000, 0)
INSERT [dbo].[Salary] ([Id], [IdPos], [Pay], [Bonus]) VALUES (N'SL03', N'PO03', 6000000, 15)
INSERT [dbo].[Salary] ([Id], [IdPos], [Pay], [Bonus]) VALUES (N'SL04', N'PO04', 300000, 10)
INSERT [dbo].[Staff] ([Id], [Name], [IdPos], [PhoneNumb], [Address], [IdAccount]) VALUES (N'ST00000002', N'LeeSin', N'PO01', N'113', N'HN', N'AC00000002')
INSERT [dbo].[Staff] ([Id], [Name], [IdPos], [PhoneNumb], [Address], [IdAccount]) VALUES (N'ST00000003', N'Nasus', N'PO01', N'114', N'HN', N'AC00000003')
INSERT [dbo].[Staff] ([Id], [Name], [IdPos], [PhoneNumb], [Address], [IdAccount]) VALUES (N'ST00000004', N'Ashe', N'PO01', N'115', N'Frejoird', N'AC00000004')
INSERT [dbo].[Staff] ([Id], [Name], [IdPos], [PhoneNumb], [Address], [IdAccount]) VALUES (N'ST00000005', N'Long', N'PO01', N'116', N'Long An', N'AC00000005')
INSERT [dbo].[Staff] ([Id], [Name], [IdPos], [PhoneNumb], [Address], [IdAccount]) VALUES (N'ST00000006', N'Nam', N'PO02', N'117', N'Vinh', N'AC00000006')
INSERT [dbo].[Staff] ([Id], [Name], [IdPos], [PhoneNumb], [Address], [IdAccount]) VALUES (N'ST00000007', N'Name', N'PO03', N'123', N'Void', N'AC00000007')
INSERT [dbo].[Staff] ([Id], [Name], [IdPos], [PhoneNumb], [Address], [IdAccount]) VALUES (N'ST01', N'Hoàng Hà', N'PO01', N'0382886866', N'Hà Nội', N'AC01')
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK__Account__IdAccou__398D8EEE] FOREIGN KEY([IdAccountType])
REFERENCES [dbo].[AccountType] ([Id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK__Account__IdAccou__398D8EEE]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK__Bill__IdCustomer__4E88ABD4] FOREIGN KEY([IdCustomer])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK__Bill__IdCustomer__4E88ABD4]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK__Bill__IdStaff__4D94879B] FOREIGN KEY([IdStaff])
REFERENCES [dbo].[Staff] ([Id])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK__Bill__IdStaff__4D94879B]
GO
ALTER TABLE [dbo].[BillInfo]  WITH CHECK ADD  CONSTRAINT [FK__BillInfo__IdBill__5070F446] FOREIGN KEY([IdBill])
REFERENCES [dbo].[Bill] ([Id])
GO
ALTER TABLE [dbo].[BillInfo] CHECK CONSTRAINT [FK__BillInfo__IdBill__5070F446]
GO
ALTER TABLE [dbo].[BillInfo]  WITH CHECK ADD  CONSTRAINT [FK__BillInfo__IdProd__5165187F] FOREIGN KEY([IdProduct])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[BillInfo] CHECK CONSTRAINT [FK__BillInfo__IdProd__5165187F]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK__Customer__Saving__45F365D3] FOREIGN KEY([IdType])
REFERENCES [dbo].[CustomerType] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK__Customer__Saving__45F365D3]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK__Product__Manufac__4AB81AF0] FOREIGN KEY([IdCategory])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK__Product__Manufac__4AB81AF0]
GO
ALTER TABLE [dbo].[Salary]  WITH CHECK ADD FOREIGN KEY([IdPos])
REFERENCES [dbo].[Position] ([Id])
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK__Staff__IdAccount__412EB0B6] FOREIGN KEY([IdAccount])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK__Staff__IdAccount__412EB0B6]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD FOREIGN KEY([IdPos])
REFERENCES [dbo].[Position] ([Id])
GO
/****** Object:  StoredProcedure [dbo].[ChangePass]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangePass]
@id NVARCHAR(10), @newPass VARCHAR(MAX)
AS
BEGIN
	UPDATE dbo.Account SET Password = @newPass WHERE Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[CheckOutBill]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckOutBill]
@idBill INT, @total INT, @discount INT, @checkOutTime SMALLDATETIME
AS
BEGIN
	--cập nhật bill
	UPDATE dbo.Bill
	SET Sum = @total, DateCheckOut = @checkOutTime, Discount = @discount
	WHERE Id = @idBill

	DECLARE @idCustomer VARCHAR(10)
	SELECT @idCustomer =IdCustomer FROM dbo.Bill WHERE Id = @idBill

	DECLARE @idCustomerType VARCHAR(10)
	SELECT @idCustomerType = IdType FROM dbo.Customer WHERE Id = @idCustomer

	DECLARE @finalPay INT = @total * (1 - @discount/100)

	-- tăng điểm tích luỹ
	UPDATE dbo.Customer
	SET Saving += @finalPay * 0.05
	WHERE Customer.Id = @idCustomer
END
GO
/****** Object:  StoredProcedure [dbo].[DisplayBill]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DisplayBill]
@IdBill VARCHAR(10) 
AS
BEGIN
	SELECT dbo.Product.Id, dbo.Product.Name, dbo.Product.SellPrice, dbo.BillInfo.Quantity  FROM dbo.Product INNER JOIN dbo.BillInfo  ON Product.Id = dbo.BillInfo.IdProduct WHERE IdBill = @IdBill
END
GO
/****** Object:  StoredProcedure [dbo].[DisplayBillByDate]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DisplayBillByDate]
@fromDate DATETIME, @toDate DATETIME
AS
BEGIN
	SELECT * FROM Bill WHERE DateCheckOut >= @fromDate AND DateCheckOut <= @toDate
END
GO
/****** Object:  StoredProcedure [dbo].[GetBillPageByDate]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBillPageByDate]
	@fromDate DATE, @toDate DATE
AS
BEGIN
	SELECT COUNT(*)
	FROM dbo.Bill WHERE DateCheckOut >= @fromDate AND DateCheckOut <= @toDate
END
GO
/****** Object:  StoredProcedure [dbo].[GetBillStatByBillId]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBillStatByBillId]
@idBill INT
AS
BEGIN
	SELECT Product.Id AS IdProduct, Name, dbo.Product.SellPrice, Quantity FROM dbo.BillInfo INNER JOIN dbo.Product ON Product.Id = BillInfo.IdProduct WHERE dbo.BillInfo.IdBill = @idBill
END
GO
/****** Object:  StoredProcedure [dbo].[GetListBillByDateAndPage]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetListBillByDateAndPage]
@fromDate DATETIME, @toDate DATETIME, @page int
AS
BEGIN
	DECLARE @pageRows INT = 10 --number of row in a page
	DECLARE @selectRows INT = @pageRows
	DECLARE @exceptRows INT = (@page - 1) * @pageRows;

	WITH BillShow AS (SELECT* FROM dbo.Bill WHERE DateCheckOut >= @fromDate AND DateCheckOut <= @toDate)
	
	SELECT TOP (@selectRows) * FROM BillShow WHERE BillShow.Id NOT IN (SELECT TOP (@exceptRows) BillShow.Id FROM BillShow)
END
GO
/****** Object:  StoredProcedure [dbo].[GetListBillInfoByBillId]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetListBillInfoByBillId]
@idBill INT
AS
BEGIN
	SELECT * FROM dbo.BillInfo WHERE IdBill = @idBill
END
GO
/****** Object:  StoredProcedure [dbo].[InsertBill]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertBill]
@idStaff VARCHAR(10),
@idCustomer VARCHAR(10)
AS
BEGIN
	INSERT dbo.Bill(Sum,IdStaff,IdCustomer, Discount)
	VALUES(   0, @idStaff, @idCustomer, 0)
END
GO
/****** Object:  StoredProcedure [dbo].[InsertBillInfo]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertBillInfo]
@idBill INT, @idProduct VARCHAR(10), @quantity INT, @sellPrice INT
AS
BEGIN
	DECLARE @isExisted INT, @productCount INT

	SELECT @isExisted = b.Id, @productCount = b.Quantity FROM dbo.BillInfo AS b
	WHERE b.IdBill = @idBill AND b.IdProduct = @idProduct

	IF(@isExisted > 0)
	BEGIN
		DECLARE @newQuantity INT = @productCount + @quantity;
		IF(@newQuantity > 0)
			UPDATE dbo.BillInfo SET Quantity = @newQuantity WHERE IdBill = @idBill AND IdProduct = @idProduct
		ELSE
			DELETE dbo.BillInfo WHERE IdBill = @idBill AND IdProduct = @idProduct
	END

	ELSE
	BEGIN
		IF(@quantity > 0)
			INSERT dbo.BillInfo(IdBill,IdProduct,Quantity,SellPrice)VALUES( @idBill, @idProduct, @quantity, @sellPrice)
    END

END
GO
/****** Object:  StoredProcedure [dbo].[InsertCustomer]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertCustomer]
@id VARCHAR(10), @name NVARCHAR(30), @idType VARCHAR(10), @phoneNumb VARCHAR(11), @saving int
AS
BEGIN
	INSERT dbo.Customer( Id,Name,IdType, PhoneNumb,Saving) VALUES( @id, @name, @idType, @phoneNumb, 0)
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateStaffInfo]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateStaffInfo]
@address NVARCHAR(20), @phone VARCHAR(11), @id VARCHAR(10)
AS
BEGIN
	UPDATE dbo.Staff
	SET PhoneNumb = @phone, Address = @address WHERE Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[UserLogin]    Script Date: 11/3/2019 10:37:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UserLogin]
@userName VARCHAR(10), @password VARCHAR(10)
AS
BEGIN
	SELECT * FROM dbo.Account WHERE Name = @userName AND Password = @password
END
GO
USE [master]
GO
ALTER DATABASE [ShopManagement] SET  READ_WRITE 
GO
